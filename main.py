from datetime import (
    datetime,
    timedelta,
)
from typing import cast
import uvicorn
from fastapi import (
    FastAPI,
    HTTPException,
    Request,
    status,
)
from fastapi.responses import JSONResponse
from fastapi_utils.tasks import repeat_every
from services.database.db_functions import (
    get_card,
    get_users_today_missed,
    update_time_send,
    update_previous_card_id,
)

from services.telegram.toro_card_bot import (
    start_telegram_bot,
    send_message,
    setup_bot_commands,
)
from services.database.db_models import User
from modules import Config
from models import (
    Card,
    OperationResult,
    OperationResultContent,
)
from routes.endpoints import router

from services.images import CardImage

config: Config = Config()
app: FastAPI = FastAPI()
app.include_router(router)


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    response = await call_next(request)
    return response


@app.get("/")
async def health_check() -> JSONResponse:
    return JSONResponse(status_code=status.HTTP_200_OK, content={"status": "ok"})


@app.exception_handler(HTTPException)
async def http_exception_handler(_: Request, exc: HTTPException) -> JSONResponse:
    """ Обработка вызванных исключений и формирование однотипного ответа """
    return OperationResult.error(
        error_message=exc.detail,
        status_code=exc.status_code,
    )


@repeat_every(seconds=10)
async def periodic_send():
    users: list[User] = get_users_today_missed()
    for user in users:
        card = get_card(cast(int, user.id_from_tg))
        image = CardImage.get_image_by_id(id=cast(int, card.id))
        if image != bytes():
            await send_message(
                user_id=cast(int, user.id_from_tg),
                card=card,
                image=image,
            )
        next_dt: datetime = datetime.now() + timedelta(seconds=10)
        update_time_send(
            id_from_tg=cast(int, user.id_from_tg),
            next_send_time=next_dt,
        )
        update_previous_card_id(
            id_from_tg=cast(int, user.id_from_tg),
            card_id=cast(int, card.id),
        )


@app.on_event("startup")
async def on_startup():
    await start_telegram_bot()
    await setup_bot_commands()
    await periodic_send()


if __name__ == "__main__":
    uvicorn.run("main:app",
                host=config.APP_HOST,
                port=int(config.APP_PORT),
                log_level=config.log_level,
                access_log=False,
                workers=1)
