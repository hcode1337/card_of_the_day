from typing import Literal, Optional
from fastapi import status
from fastapi.responses import JSONResponse
from pydantic import BaseModel


class OperationResultContent(BaseModel):
    """ Модель результата выполнения операции"""
    status: Literal["ok", "error"]
    error_message: Optional[str] = ""


class OperationResult(JSONResponse):
    @staticmethod
    def ok() -> JSONResponse:
        """ Формирование ответа успешного выполнения"""
        return JSONResponse(
            status_code=status.HTTP_200_OK,
            content=OperationResultContent(status="ok").dict(),
        )

    @staticmethod
    def error(status_code: int, error_message: str) -> JSONResponse:
        """ Формирование ответа ошибки """
        return JSONResponse(
            status_code=status_code,
            content=OperationResultContent(
                    status="error",
                    error_message=error_message,
                ).dict(),
        )
