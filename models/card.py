from typing import Optional
from pydantic import BaseModel
from fastapi import UploadFile, File


class Card(BaseModel):
    id: Optional[int]
    label: str
    description: Optional[str]
