# Карта дня



## О проекте
...

## Ссылки
- [Доска](https://www.notion.so/9231e185f0d2478ab009c3171e1d647a?v=c2a4539230c340f9bd1570033a7a9eba&p=e3be3e4ef32541ecb645f4458f4e704b&pm=s)

### Инструкции
- Создал директории для хранения данных БД и файлового хралища 
    - /var/lib/postgresql
    - /var/lib/minio
- Postgresql (текущая версия 15.2)
    ```bash
        docker run -d --name postgres -e POSTGRES_PASSWORD=SECRET -e POSTGRES_USER=USER_NAME --network host --volume /var/lib/postgresql:/var/lib/postgresql postgres
    ```
- Minio S3
    ```bash
        docker run -d --name minio --network host --volume /var/lib/minio:/data quay.io/minio/minio server /data

    ```
#### Без Docker
- Установить python >= 3.8.5
- (не нужно если используйте PyCharm) Установить виртуальное окружение https://python.land/virtual-environments/virtualenv
- (если не стоит pip) python -m ensurepip
- Установить необходимые библиотеки
```
pip install -r requirements.txt
```
- Создать файл .env в текущей папке 
- Указать токен полученный с помощью BotFather в файле .env. В формате
TELEGRAM_TOKEN=ваш токен из BotFather
- Открыть терминал (cmd к примеру)
- Перейти в папку с кодом (cd <путь к папке>)
-  (не нужно если используйте PyCharm) изменить PYTHONPATH (
Windows:
- открываем cmd.exe
- set PYTHONPATH=.
Linux:
- export PYTHONPATH=.
  )
- Запустить скрипт работы с api
```
python main.py
```
- Открыть другой терминал (cmd к примеру)
- Запустить скрипт работы с telegram. 
windows:
```
python .\services\telegram\toro_card_bot.py
```
linux:
```
python ./services/telegram/toro_card_bot.py
```
# Полезные команды
- Обновить зависимости (список используемых библиотек):
```
pip freeze > requirements.txt
```

http://127.0.0.1:8080/welcome

swagger:
http://127.0.0.1:8080/docs

telegram updates:
https://api.telegram.org/bot<token>/getUpdates

flake8:
flake8 --exclude venv,docs --ignore=F401

нужно указать токен в docker-compose.yml