import asyncio
from typing import cast
from aiogram import (
    Bot,
    Dispatcher,
    types,
)
from aiogram.types import BotCommand
from aiogram.types import mixins
from aiogram.types.inline_keyboard import InlineKeyboardButton, InlineKeyboardMarkup

from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from models.card import Card
from modules.config import Config
from services.database.db_functions import (
    create_user,
    get_card_by_id,
    stop_for_user, shuffle_cards,
)

from aiogram.utils.exceptions import (
    MessageCantBeDeleted,
    MessageToDeleteNotFound,
)
from aiogram.types.message import ParseMode
from aiogram.utils.callback_data import CallbackData
import logging
import os
import pathlib
from services.database.db_functions import create_card
import datetime
import random
import string
from services.images import CardImage
from cashews import cache
from aiogram.utils.exceptions import ChatNotFound
from contextlib import suppress

config: Config = Config()
bot = Bot(token=config.TELEGRAM_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(
    bot,
    storage=storage,
)
vote_cb = CallbackData('vote', 'action', 'card_id')

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
    filename="telegram_bot.log"
)
logger = logging.getLogger(__name__)

cache.setup("mem://")


class TgCardStates(State):
    async def set(self, user=None):
        """Option to set state for concrete user"""
        state = Dispatcher.get_current().current_state(user=user)
        await state.set_state(self.state)


class TgmCardStates(StatesGroup):
    init_add_card = TgCardStates()
    second_add_photo = TgCardStates()
    description = TgCardStates()


def _is_admin(user_id):
    return True
    # return user_id in [1111123725, 787564993]


@dp.message_handler(commands='add')
async def add(
        message: types.Message,
        state: FSMContext,
):
    logger.info("start execution add command")
    user_id = message.chat.id
    if _is_admin(user_id):
        try:
            await state.finish()
            logger.debug("reply message about add command")
            await message.reply(
                text=config.ADD_CMD_TEXT,
                parse_mode=ParseMode.MARKDOWN,
            )
            logger.debug("set state in add command")
            await TgmCardStates.init_add_card.set(user_id)
        except Exception:
            logger.error('Error at add command', exc_info=True)
            await message.reply(
                text=config.CMD_NOT_SUPPORTED,
                parse_mode=ParseMode.MARKDOWN,
            )
    else:
        await message.reply(
            text=config.CMD_NOT_SUPPORTED,
            parse_mode=ParseMode.MARKDOWN,
        )


@dp.message_handler(commands=['stop'])
async def stop(message: types.Message):
    await message.reply(config.STOPPED_TEXT)
    stop_for_user(id_from_tg=message.chat.id)


@dp.message_handler(commands=['get'])
async def get_card(message: types.Message):
    arguments = message.get_args()
    await message.reply(f"card number:{arguments}")
    try:
        card: Card = get_card_by_id(int(arguments))
        user_id = message.chat.id
        await bot.send_photo(
            chat_id=user_id,
            caption=card.label,
            photo=CardImage.get_image_by_id(card.id),
        )
        await message.reply(card.description)
    except Exception as exception:
        logger.exception(exception)


@dp.message_handler(commands=['delete'])
async def command_delete_message(message: types.Message):
    arguments = message.get_args().split()
    if len(arguments) != 2:
        await message.reply(
            config.DELETE_INVALID_COUNT_PARAMETERS
        )
    else:
        await message.reply(
            f"delete user_id:{arguments[0]} message_id:"
            f"{arguments[1]}"
        )
        await delete_message(int(arguments[0]), int(arguments[1]))


@dp.message_handler(commands=['start', 'help'])
async def welcome(message: types.Message):
    user_id = message.chat.id
    logger.debug(f"user id {user_id}")
    if _is_admin(user_id):
        msg = await message.reply(
            # config.START_AS_ADMIN_TEXT.format(
            #     config.SUPPORT_CONTACT,
            # )
            config.START_AS_USER_TEXT,
        )
        message_id = msg["message_id"]
        logger.debug(f"message id {message_id}")
    else:
        await message.reply(config.START_AS_USER_TEXT)
    create_user(id_from_tg=message.chat.id)


@dp.message_handler(commands=['shuffle'])
async def start_shuffle(message: types.Message):
    shuffle_cards()
    await message.reply("Карты перемешаны")


@dp.callback_query_handler(vote_cb.filter(action='show'))
async def vote_up_cb_handler(query: types.CallbackQuery, callback_data: dict):
    card: Card = get_card_by_id(int(callback_data['card_id']))
    description: str = f"{card.label}\n{card.description}"
    await bot.edit_message_caption(
        query.from_user.id,
        query.message.message_id,
        caption=description,
    )


def _get_random_text(length=5):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


def generate_filename(folder: pathlib.Path) -> str:
    template_str = "test - {date: %Y_%m_%d_%H_%M_%S}.tmp"
    init_filename = os.path.join(
        folder,
        template_str.format(date=datetime.datetime.now()),
    )
    _i = 0
    while (
            os.path.exists(init_filename)
    ):
        _i += 1
        logger.warning("file exists")
        logger.debug(init_filename)
        template_str = _get_random_text() + "- {date: %Y_%m_%d_%H_%M_%S}.txt"
        init_filename = os.path.join(
            folder,
            template_str.format(date=datetime.datetime.now()),
        )
        if _i > 100:
            return os.path.join(folder, "000.tmp")
        logger.debug("generate file:")
        logger.debug(init_filename)
    return init_filename


async def _save_image(
        fileobject: mixins,
        label: string,
        user_id,
) -> bytes:
    logger.debug("start _read_image")
    image_path = generate_filename(
        pathlib.Path(
            os.path.dirname(os.path.realpath(
                __file__)
            )
        ).parent.parent
    )
    logger.debug("_read_image. file path:")
    logger.debug(image_path)
    await fileobject.download(destination_file=image_path)
    logger.debug("Bot download file successfully")
    await cache.set(
        key=str(user_id),
        value=label,
        expire="1h",
    )
    await cache.set(
        key=f"file_{user_id}",
        value=image_path,
        expire="1h",
    )


def _create_card_id(label, description, image_path) -> int:
    card_id = create_card(
        Card(
            label=label,
            description=description
        )
    )
    logger.info(f"card id:{card_id}")
    with open(image_path, "rb") as file_handler:
        content = file_handler.read()
        logger.debug("tmp file read successfully")
        CardImage.save_image(card_id, content)
    os.remove(image_path)
    dirname = os.path.dirname(image_path)
    logger.debug("list tmp files:")
    logger.debug(os.listdir(dirname))
    return card_id


@dp.message_handler(state=TgmCardStates.init_add_card,
                    content_types=['document'],
                    )
async def upload_photo(message: types.Message, state: FSMContext):
    logger.info("start execution upload_photo command")
    logger.debug(f"content type: {message.content_type}")
    user_id = message.chat.id
    try:
        if message.caption is None or len(message.caption) == 0:
            logger.info("finish execution upload_photo with error")
            logger.error("label is absent")
            await message.reply(
                text=config.ERROR_LABEL,
                parse_mode=ParseMode.MARKDOWN,
            )
            await state.finish()
        else:
            new_file = await message.document.get_file()
            label = message.caption
            await _save_image(new_file, label, user_id)

            await TgmCardStates.next()
            logger.info(f"label is:{label}")
            await message.reply(
                text=config.NEXT_PHOTO_TEXT,
                parse_mode=ParseMode.MARKDOWN,
            )
            await message.reply(
                text=config.AFTER_PHOTO_TEXT,
                parse_mode=ParseMode.MARKDOWN,
            )
    except Exception:
        await message.reply(
            text=config.CANT_READ_PHOTO,
            parse_mode=ParseMode.MARKDOWN,
        )
        logger.error('Error at upload_photo', exc_info=True)
        await TgmCardStates.init_add_card.set(user_id)


@dp.message_handler(state=TgmCardStates.init_add_card,
                    content_types=['photo'],
                    )
async def upload_compressed_photo(message: types.Message, state: FSMContext):
    logger.info("start execution upload_compressed_photo command")
    logger.debug(f"content type: {message.content_type}")
    user_id = message.chat.id
    try:
        if message.caption is None or len(message.caption) == 0:
            logger.info("finish execution upload_compressed_photo with error")
            logger.error("label is absent")
            await message.reply(
                text=config.ERROR_LABEL,
                parse_mode=ParseMode.MARKDOWN,
            )
            await state.finish()
        else:
            logger.debug("caption exists")
            filename = await message.photo[0].get_file()
            label = message.caption
            await _save_image(filename, label, user_id)
            await TgmCardStates.next()
            logger.info(f"label is:{label}")
            await message.reply(
                text=config.NEXT_PHOTO_TEXT,
                parse_mode=ParseMode.MARKDOWN
            )
            await message.reply(
                text=config.AFTER_PHOTO_TEXT,
                parse_mode=ParseMode.MARKDOWN
            )
    except Exception:
        await message.reply(text=config.CANT_READ_PHOTO,
                            parse_mode=ParseMode.MARKDOWN)
        logger.error('Error at upload_compressed_photo', exc_info=True)
        await TgmCardStates.init_add_card.set(user_id)


@dp.message_handler(state=TgmCardStates.second_add_photo)
async def upload_description(
        message: types.Message,
        state: FSMContext):
    user_id = message.chat.id
    if _is_admin(user_id):
        description = message.text
        label = await cache.get(str(user_id))
        image_path = await cache.get(f"file_{user_id}")
        card_id = _create_card_id(
            label=label,
            description=description,
            image_path=image_path
        )
        logger.info(f"for user:{user_id}")
        await state.finish()
        await message.reply(
            text=config.SUCCESS_ADD_TEXT.format(
                str(card_id)),
            parse_mode=ParseMode.MARKDOWN
        )
        logger.info(f"description is:{description}")


async def setup_bot_commands():
    bot_commands = [
        BotCommand(command="/start", description="Запуск рассылки сообщений"),
        BotCommand(command="/stop", description="Останов рассылки сообщений"),
    ]
    await bot.set_my_commands(bot_commands)


async def delete_message(user_id: int, message_id: int):
    with suppress(MessageCantBeDeleted, MessageToDeleteNotFound):
        await bot.delete_message(user_id, message_id)


async def send_message(user_id: int, card: Card, image: bytes):
    inline_keyboard: list[list[InlineKeyboardButton]] = [[
        InlineKeyboardButton(
            # text=cast(str, card.label),
            text="Подробнее",
            callback_data=vote_cb.new(action='show', card_id=card.id),
        ),
    ]]
    try:
        msg = await bot.send_photo(
            chat_id=user_id,
            photo=image,
            caption=cast(str, card.label),
            reply_markup=InlineKeyboardMarkup(
                inline_keyboard=inline_keyboard,
            ),
        )
        message_id = msg["message_id"]
        logger.debug(f"photo message id {message_id}")
    except ChatNotFound:
        pass


async def start_telegram_bot():
    asyncio.create_task(dp.start_polling(bot))
