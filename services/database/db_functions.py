import logging
import random
from datetime import datetime, timedelta

from models import Card
from .db_main import get_session, engine
from .db_models import CardTable, User, RandomRow, Base

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO,
    # filename="main_view_logger.log"
)
logger = logging.getLogger(__name__)


def create_card(card: Card) -> int:
    """
    Cохранение карты
    :param card: модель класса Card
    :return: id созданной карты
    """
    session = get_session()
    session.expire_on_commit = False
    new_card = CardTable(
        label=card.label,
        description=card.description,
    )
    try:
        session.add(new_card)
        session.commit()
        new_card_random_row = RandomRow(
            card_id=new_card.id,
        )
        session.add(new_card_random_row)
        session.commit()
    except Exception as e:
        logger.error(f"{e.__class__.__name__}: {e}")
        session.rollback()
    return new_card.id


def edit_card(card: Card) -> None:
    """
    Изменение карты
    :param card: модель класса Card
    :return:
    """
    session = get_session()
    cardtable = session.query(CardTable).filter_by(id=card.id).one_or_none()
    if cardtable is not None:
        try:
            if card.label is not None:
                cardtable.label = card.label
            if card.description is not None:
                cardtable.description = card.description
            session.commit()
        except Exception as e:
            logger.error(f"{e.__class__.__name__}: {e}")
            session.rollback()
    return None


def get_card_by_id(card_id: int) -> Card:
    """
    Получение одной карты
    :param id_from_tg: id карты
    :return: модель класса Card
    """
    session = get_session()
    cardtable = session.query(CardTable).filter_by(id=card_id).first()
    if not cardtable:
        return Card(label="Not found")

    card = Card(
        id=cardtable.id,
        label=cardtable.label,
        description=cardtable.description,
    )
    return card


def get_card(id_from_tg: int) -> Card:
    """
    Получение следующей к показу карты
    :param id_from_tg: id пользователя телеграмм
    :return: модель класса Card.
    """
    session = get_session()
    user_info = session.query(User).filter_by(id_from_tg=id_from_tg).one_or_none()
    current_row = session.query(RandomRow).filter_by(card_id=user_info.card_received_count).one_or_none()
    next_row = session.query(RandomRow).filter(RandomRow.id > current_row.id).first()
    if next_row is None:
        next_row = session.query(RandomRow).order_by(RandomRow.id).first()
    cardtable = session.query(CardTable).filter_by(id=next_row.card_id).one_or_none()
    card = Card(
        id=cardtable.id,
        label=cardtable.label,
        description=cardtable.description,
    )
    return card


def stop_for_user(id_from_tg: int) -> None:
    """
    Остановка рассылки карт для указанного пользователя
    :param id_from_tg: id пользователя телеграмм
    :return:
    """
    session = get_session()
    user_info = session.query(User).filter_by(id_from_tg=id_from_tg).one_or_none()
    if user_info:
        user_info.next_send_time = datetime.now() + timedelta(days=365*365)
        session.commit()


def create_user(id_from_tg: int) -> None:
    """
    Сохранения ИД нового пользователя
    :param id_from_tg: id пользователя телеграмм
    :return:
    """
    session = get_session()
    user_info = session.query(User).filter_by(id_from_tg=id_from_tg).one_or_none()
    if user_info:
        user_info.next_send_time = datetime.now()
    else:
        user_create = User(
            id_from_tg=id_from_tg,
            card_received_count=get_random_card().id,
            next_send_time=datetime.now(),
            role_id=1
        )
        session.add(user_create)
    session.commit()
    return None


def update_time_send(id_from_tg: int, next_send_time: datetime) -> None:
    """
    Изменение времени отправки карты
    :param id_from_tg: id пользователя телеграмм
    :param next_send_time: время в формате time
    :return:
    """
    session = get_session()
    user_info = session.query(User).filter_by(id_from_tg=id_from_tg).one_or_none()
    if user_info is not None:
        user_info.next_send_time = next_send_time
        session.commit()
    return None


def update_previous_card_id(id_from_tg: int, card_id: int) -> None:
    """
    Запись последней показанной карты для конкретного пользователя
    :param card_id: id карты
    :param id_from_tg: id пользователя телеграмм
    :param next_send_time: время в формате time
    :return:
    """
    session = get_session()
    user_info = session.query(User).filter_by(id_from_tg=id_from_tg).one_or_none()
    if user_info is not None:
        user_info.card_received_count = card_id
        session.commit()
    return None


def get_users_today_missed() -> list[User]:
    """
    Получить идентификаторы пользователей,
    для которых на сегодня пропущена отправка карт
    :return: результат выборки в формате модели User
    """
    session = get_session()
    today_datetime = datetime.now()
    users_info = session.query(User).filter(User.next_send_time<=today_datetime)
    users_info = users_info.all()
    return users_info


def shuffle_cards() -> None:
    session = get_session()
    Base.metadata.create_all(engine)
    cards = session.query(CardTable).filter_by(deleted=False).all()
    if cards:
        list_id = []
        for card in cards:
            list_id.append(card.id)
        random.shuffle(list_id)
        session.query(RandomRow).delete()
        for card_id in list_id:
            row = RandomRow(
                card_id=card_id,
            )
            session.add(row)
        session.commit()


def get_random_card() -> Card:
    session = get_session()
    cards = session.query(RandomRow).all()
    card_id_list = []
    for card in cards:
        card_id_list.append(card.id)
    card_id = random.choice(card_id_list)
    card = session.query(CardTable).filter_by(id=card_id).one_or_none()
    if card:
        card = Card(
            id=card.id,
            label=card.label,
            description=card.description,
        )
    return card