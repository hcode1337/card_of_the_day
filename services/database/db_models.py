from datetime import datetime

from sqlalchemy import (
    Column,
    Integer, String, ForeignKey, DateTime, Boolean, BigInteger, Time,
)
from sqlalchemy.ext.declarative import declarative_base


class Base:
    id = Column(Integer, primary_key=True)

    def __repr__(self):
        return str(self)


Base = declarative_base(cls=Base)  # type: ignore


class TimestampMixin:
    created_at = Column(DateTime, nullable=True, default=datetime.utcnow)


class CardTable(TimestampMixin, Base):
    __tablename__ = "cardtables"

    label = Column(String(200), unique=False, nullable=False)
    description = Column(String(200), unique=False, nullable=False)
    deleted = Column(Boolean, unique=False, nullable=False, default=False)

    def __repr__(self) -> str:
        return "=" * 50 + str(self.label)


class User(TimestampMixin, Base):
    __tablename__ = "users"

    id_from_tg = Column(BigInteger, unique=True, nullable=False)
    card_received_count = Column(Integer, unique=False, nullable=False, default=0)
    next_send_time = Column(DateTime, unique=False, nullable=False, default=None)
    deleted = Column(Boolean, unique=False, nullable=False, default=False)
    role_id = Column(Integer, ForeignKey('roles.id'), nullable=False, default=1)

    def __repr__(self) -> str:
        return "*" * 50 + str(self.id_from_tg)


class Role(TimestampMixin, Base):
    __tablename__ = "roles"

    role_name = Column(String(200), unique=False, nullable=True)
    access_level = Column(Integer, unique=False, nullable=False)
    deleted = Column(Boolean, unique=False, nullable=False, default=False)


class RandomRow(TimestampMixin, Base):
    __tablename__ = "randomrows"

    """
    Модель таблицы для хранения рандомной последовательности карт 
    """

    card_id = Column(Integer, ForeignKey('cardtables.id'), nullable=False, default=0)