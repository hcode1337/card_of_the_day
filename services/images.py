from fastapi.responses import JSONResponse
from fastapi import status
import mimetypes
import magic
import os

from models import OperationResult
from modules.config import image_path


class CardImage:

    @staticmethod
    def _prepare_directory():
        """
        Проверяем существует ли директория, если нет создаем
        :return: возврата нет, работает по прицнипу процедуры
        """
        is_exist = os.path.exists(image_path)
        if not is_exist:
            os.makedirs(image_path)

    @staticmethod
    def _get_file_extension(image: bytes) -> str:
        """
        Проверяем mime type файла на допустимые значения
        :param image: байты картинки
        :return: возвращает mime type файла
        """
        mime = magic.Magic(mime=True)
        return mimetypes.guess_extension(mime.from_buffer(image))

    @staticmethod
    def save_image(card_id: int, image_bytes: bytes) -> JSONResponse:
        """
        Метод для сохранения картинки в папку images
        :param image_path: путь к директории картинки
        :return: возврат результата сохранения картинки в папку images
        """
        CardImage._prepare_directory()

        try:
            file_extension = CardImage._get_file_extension(image_bytes)
            if file_extension in ['.png', '.jpg', '.jpeg']:
                with open(f"{image_path}/{card_id}{file_extension}", "wb") as file:
                    file.write(image_bytes)
                return OperationResult.ok()
            else:
                return OperationResult.error(
                    error_message="Передайте картинку в допустимом формате",
                    status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
                )
        except Exception as e:
            return OperationResult.error(
                error_message=str(e),
                status_code=status.HTTP_404_NOT_FOUND
            )

    @staticmethod
    def get_image_by_id(id: int) -> bytes:
        """
        Метод поиска локально картинки по card.id
        :param image_path: путь к директории картинки
        :return: возвращает каритнку в bytes
        """
        try:
            file_name: str = f"{image_path}/{id}.jpg"
            if not os.path.exists(file_name):
                raise Exception()
            with open(file_name, "rb") as image:
                f = image.read()
                return bytearray(f)
        except Exception as e:
            return bytearray()
