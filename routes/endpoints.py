from fastapi import (
    APIRouter,
    HTTPException,
    status,
    File,
    UploadFile,
    Depends
)

from fastapi.responses import JSONResponse
from modules.config import ADMIN
from models.card import Card
from models.operation_result import OperationResult, OperationResultContent
from services.database.db_functions import get_card_by_id, create_card, edit_card
from services.images import CardImage

router = APIRouter()
ADMIN = True
# после добавления авторизации и тестирования убрать эту заглушку ADMIN = True


@router.get("/get_card/",
            response_model=Card,
            summary="Получение карточки по идентификатору",
            tags=["card"]
            )
async def get_card(card_id: int) -> JSONResponse:
    """
    Метод позволяющий получить карту из базы данных \n
    :card_id: идентификатор карты \n
    :return: возвращает карту из базы данных
    """
    res = get_card_by_id(card_id)

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=res.dict(),
    )


@router.post("/add_card/",
             summary="Добавлени карты",
             tags=["card"],
             )
async def add_card(card: Card = Depends(), file: UploadFile = File(...)):
    """
    Метод позволяющий добавить карту в базу данных, изображение добавляется в корень проекта, \n
    в папку iamges с соответсвующим номером \n
    :param card: запрос который содержит label, description \n
    :param file: изображение карты \n
    :return: возвращает карту
    """

    if ADMIN:
        card_image = file.file.read()

        card_id = create_card(card)
        CardImage.save_image(card_id, card_image)

        return JSONResponse(
            status_code=status.HTTP_200_OK,
            content=card.json(),

        )
    else:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Необходимо авторизоваться как администратор"
        )


@router.post("/edit_card/",
             response_model=OperationResultContent,
             summary="Редактирование карты",
             tags=["card"]
             )
async def edit_card_by_id(card: Card) -> JSONResponse:
    """
    Метод позволяющий изменить карту в базе данных, изображение изменить нельзя! \n
    :card: передаем данные image, label, description, card_id \n
    :return: результат выполнения запроса
    """

    if ADMIN:
        edit_card(card)
        return OperationResult.ok()
    else:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Необходимо авторизоваться как администратор"
        )
